export class VoteitupClientPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('voteitup-client-app h1')).getText();
  }
}
