import { VoteitupClientPage } from './app.po';

describe('voteitup-client App', function() {
  let page: VoteitupClientPage;

  beforeEach(() => {
    page = new VoteitupClientPage();
  })

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('voteitup-client works!');
  });
});
