import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {VoteitupClientModule} from './app/voteitup-client.module';

platformBrowserDynamic().bootstrapModule(VoteitupClientModule);
