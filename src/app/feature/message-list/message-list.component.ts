import {Component, OnInit, Input} from '@angular/core';
import {DataService} from '../../shared/data.service';
import {IMessage} from '../../model/IMessage';
import {MessageNewComponent} from '../message-new/message-new.component';
import {Subscription} from "rxjs/Rx";

@Component({
  moduleId: module.id,
  selector: 'app-message-list',
  templateUrl: 'message-list.component.html',
  styleUrls: ['message-list.component.css'],
  directives: [MessageNewComponent]
})
export class MessageListComponent implements OnInit {

  @Input() voteId: number;
  messages: IMessage[];
  messagesSubscription: Subscription;

  constructor(private dataService: DataService) {
  }

  ngOnInit() {
    this.messagesSubscription = this.dataService.messages.subscribe(messages => {
      this.messages = messages.filter(m => {
        return m.voteId === this.voteId;
      });
    });
  }

  ngOnDestroy() {
    this.messagesSubscription.unsubscribe();
  }
}
