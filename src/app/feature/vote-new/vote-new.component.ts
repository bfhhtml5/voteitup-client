import {Component, OnInit} from '@angular/core';
import {DataService} from '../../shared/data.service';
import {IPhase} from '../../model/IPhase';
import {Router, ActivatedRoute} from '@angular/router';
import {Subscription} from "rxjs/Rx";
import {IVote} from "../../model/IVote";

@Component({
  moduleId: module.id,
  selector: 'app-vote-new',
  templateUrl: 'vote-new.component.html',
  styleUrls: ['vote-new.component.css']
})
export class VoteNewComponent implements OnInit {
  model: any = {};
  currentPhase: number = 0;
  paramsSubscription: Subscription;
  private vote: IVote;

  constructor(private dataService: DataService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.paramsSubscription = this.activatedRoute.params
      .map(p => p['id']).combineLatest(this.dataService.votes)
      .subscribe(input => {
        let idFromUrl: string = input[0];
        let voteList: IVote[] = input[1];
        this.vote = voteList.find(v => v.id.toString() === idFromUrl);
        if (this.vote) {
          this.currentPhase = this.vote.currentPhase;
          this.model = Object.assign({}, this.vote);
        }
      });
  }

  onSubmit() {
    if (this.vote) {
      this.dataService.updateVote(this.model);
      this.router.navigate(['/vote', this.model.id]);
    } else {
      this.dataService.newVote({
        id: null,
        name: this.model.name,
        description: this.model.description,
        createdBy: this.dataService.currentUser,
        ideasTo: this.model.ideasTo,
        votesTo: this.model.votesTo,
        votesPerUser: this.model.votesPerUser,
        imageUrl: this.model.imageUrl,
        currentPhase: IPhase.PLANNED
      });
      this.router.navigate(['/votes']);
    }
  }
}
