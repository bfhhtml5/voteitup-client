import {Component, OnInit, Input} from '@angular/core';
import {IIdea} from '../../model/IIdea';
import {DataService} from '../../shared/data.service';
import {IVote} from "../../model/IVote";
import {IPhase} from "../../model/IPhase";

@Component({
  moduleId: module.id,
  selector: 'app-idea-list',
  templateUrl: 'idea-list.component.html',
  styleUrls: ['idea-list.component.css']
})
export class IdeaListComponent implements OnInit {

  @Input() vote: IVote;

  ideas: IIdea[];
  iPhase: any = IPhase;

  colNum: string = 'm4';

  constructor(private dataService: DataService) {
  }

  ngOnInit() {
    this.dataService.ideas.subscribe(input => {
      this.ideas = input.filter(v => {
        return v.voteId === this.vote.id;
      });
    });

    this.setCardGrid(window.innerWidth);
  }

  onResize(event) {
    this.setCardGrid(event.target.innerWidth);
  }

  setCardGrid(windowSize: number) {
    let c = Math.round(12 / Math.round(windowSize / 400));
    if (c > 2) {
      this.colNum = 'm' + c.toString();
    }
  }

  voteUp(idea) {
    this.dataService.voteUp(this.vote.id, idea.id);
  }

  voteDown(idea) {
    this.dataService.voteDown(this.vote.id, idea.id);
  }

  removeIdea(idea) {
    this.dataService.removeIdea(idea);
  }

  getMyVoteCount(idea) {
    if (!idea.upVotes || !this.dataService.currentUser) {
      return 0;
    }

    return idea.upVotes
      .filter(i => i.createdBy.name === this.dataService.currentUser.name).length;
  }
}
