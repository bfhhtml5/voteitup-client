import { Component, OnInit } from '@angular/core';
import {IUser} from '../../model/IUser';
import {DataService} from '../../shared/data.service';
import {Router} from '@angular/router';

@Component({
  moduleId: module.id,
  selector: 'app-login',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.css']
})
export class LoginComponent implements OnInit {
  username: IUser = { name: ''};

  constructor(private dataService: DataService, private router: Router) { }

  onSubmit() {
    this.dataService.setCurrentUser(this.username);
    this.router.navigate(['/']);
  }

  ngOnInit() {
  }
}
