import {Component, OnInit, Input} from '@angular/core';
import {IIdea} from '../../model/IIdea';
import {DataService} from '../../shared/data.service';
import {CHART_DIRECTIVES} from 'ng2-charts/ng2-charts';
import {IVote} from '../../model/IVote';

@Component({
  moduleId: module.id,
  selector: 'app-bubble',
  templateUrl: 'bubble.component.html',
  styleUrls: ['bubble.component.css'],
  directives: [CHART_DIRECTIVES]
})
export class BubbleComponent implements OnInit {

  @Input() vote: IVote;
  ideas: IIdea[];

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    legend: {
      display: true,
    },
    scales: {
    xAxes: [{
        ticks: {
            beginAtZero: true,
            stepSize: 1
        }
    }]
}
  };

  public barChartLabels: any;
  public barChartType: string = 'horizontalBar';
  public barChartLegend: boolean = true;

  public barChartData: any[];

  constructor(private dataService: DataService) {
  }

  ngOnInit() {
    this.dataService.ideas.subscribe(input => {
      this.ideas = input.filter(v => {
        return v.voteId === this.vote.id;
      });

      this.ideas.sort(i => i.upVotes.length);

      this.barChartLabels = this.ideas.map(i => {
        if (i.name.length > 10) {
          return i.name.split(' ');
        } else {
          return i.name;
        };
      });
      this.barChartData = [{
        label: this.vote.name,
        backgroundColor: [],
        borderColor: [],
        borderWidth: 1,
        data: []
      }];

      this.ideas.forEach((i, index) => {
        this.barChartData[0].data.push(i.upVotes ? i.upVotes.length : 0);
        this.barChartData[0].backgroundColor.push(index % 2 === 0 ? 'rgba(255, 99, 132, 0.2)' : 'rgba(54, 162, 235, 0.2)');
        this.barChartData[0].borderColor.push(index % 2 === 0 ? 'rgba(255,99,132,1)' : 'rgba(54, 162, 235, 1)');
      });
    });
  }
}
