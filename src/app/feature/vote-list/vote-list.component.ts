import {Component, OnInit} from '@angular/core';
import {IPhase} from '../../model/IPhase';
import {IVote} from '../../model/IVote';
import {Subject, Subscription} from 'rxjs/Rx';
import {DataService} from '../../shared/data.service';
import {ActivatedRoute} from '@angular/router';
declare var $: any;

@Component({
  moduleId: module.id,
  selector: 'app-vote-list',
  templateUrl: 'vote-list.component.html',
  styleUrls: ['vote-list.component.css']
})
export class VoteListComponent implements OnInit {
  displayPhase: IPhase;
  phaseChanged: any = new Subject();
  phaseChangedSubscription: Subscription;
  iPhase: any = IPhase; // store a reference to enum to use it in html
  votes: IVote[];
  colNum: string = 'm3';

  constructor(private dataService: DataService,
              private route: ActivatedRoute) {
    // Combine the users 'click' on tabs and the server stream
    this.phaseChangedSubscription = this.phaseChanged.combineLatest(this.dataService.votes)
      .subscribe(input => {
        let phase: IPhase = input[0];
        let voteList: IVote[] = input[1];
        this.votes = voteList.filter(v => {
          return v.currentPhase === phase;
        });
      });
  }

  ngOnInit() {
    // Run jQuery to get active tab
    $('ul.tabs').tabs();

    // Set initial phase value
    this.displayPhase = IPhase.RUNNING;
    this.phaseChanged.next(IPhase.RUNNING);
    this.setCardGrid(window.innerWidth);

    // Monitor route to select correct tabs on 'browser back'
    this.route.params.map(p => p['phase']).subscribe(tab => {
      switch (parseInt(tab)) {
        case IPhase.PLANNED:
          this.setDisplayPhase(this.iPhase.PLANNED);
          $('ul.tabs').tabs('select_tab', 'PLANNED' );
          break;
        case IPhase.ENDED:
          this.setDisplayPhase(this.iPhase.ENDED);
          $('ul.tabs').tabs('select_tab', 'ENDED');
          break;
        default:
          this.setDisplayPhase(this.iPhase.RUNNING);
          $('ul.tabs').tabs('select_tab', 'RUNNING');
      }
    });
  }

  ngOnDestroy() {
    this.phaseChangedSubscription.unsubscribe();
  }

  setDisplayPhase(phase: IPhase) {
    this.displayPhase = phase;
    this.phaseChanged.next(phase);
  }

  onResize(event) {
    this.setCardGrid(event.target.innerWidth);
  }

  setCardGrid(windowSize: number) {
    let c = Math.round(12 / Math.round(windowSize / 300));
    if (c > 2) {
      this.colNum = 'm' + c.toString();
    }
  }
}
