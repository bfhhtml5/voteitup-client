import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {DataService} from '../../shared/data.service';
import {Subscription} from 'rxjs/Rx';
import {IVote} from '../../model/IVote';
import {IIdea} from '../../model/IIdea';

@Component({
  moduleId: module.id,
  selector: 'app-idea-new',
  templateUrl: 'idea-new.component.html',
  styleUrls: ['idea-new.component.css']
})
export class IdeaNewComponent implements OnInit {
  model: any = {};
  vote: IVote;
  idea: IIdea;
  paramsSubscription: Subscription;

  constructor(private dataService: DataService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.paramsSubscription = this.activatedRoute.params
      .map(p => p['id']).combineLatest(this.dataService.votes)
      .subscribe(input => {
        let idFromUrl: string = input[0];
        let voteList: IVote[] = input[1];
        this.vote = voteList.find(v => v.id.toString() === idFromUrl);
      });

    this.activatedRoute.params.map(p => p['ideaId']).combineLatest(this.dataService.ideas)
      .subscribe(input => {
        let idFromUrl: string = input[0];
        let ideaList: IIdea[] = input[1];
        this.idea = ideaList.find(i => i.id.toString() === idFromUrl);

        if (this.idea) {
          this.model = this.idea;
        }
      });
  }

  ngOnDestroy() {
    this.paramsSubscription.unsubscribe();
  }

  onSubmit() {
    if (this.idea) {
      this.dataService.updateIdea(this.idea);
    } else {
      this.dataService.newIdea({
        id: null,
        voteId: this.vote.id,
        name: this.model.name,
        createdBy: this.dataService.currentUser,
        upVotes: null
      }, this.vote.id);
    }

    this.router.navigate(['/vote', this.vote.id]);
  }
}
