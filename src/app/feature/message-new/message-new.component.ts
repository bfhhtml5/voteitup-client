import {Component, OnInit, Input} from '@angular/core';
import {DataService} from '../../shared/data.service';

@Component({
  moduleId: module.id,
  selector: 'app-message-new',
  templateUrl: 'message-new.component.html',
  styleUrls: ['message-new.component.css']
})
export class MessageNewComponent implements OnInit {

  newMessage: string;
  @Input() voteId: number;

  constructor(private dataService: DataService) {}

  addNewMessage() {
    this.dataService.newMessage(this.newMessage, this.voteId);
    this.newMessage = '';
  }

  ngOnInit() {
  }
}
