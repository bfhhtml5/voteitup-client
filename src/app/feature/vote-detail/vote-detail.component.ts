import {Component, OnInit, Input, Output} from '@angular/core';
import {IdeaListComponent} from '../idea-list/idea-list.component';
import {BubbleComponent} from '../bubble/bubble.component';
import {ActivatedRoute, Router} from '@angular/router';
import {IVote} from '../../model/IVote';
import {IPhase} from '../../model/IPhase';
import {DataService} from '../../shared/data.service';
import {MessageListComponent} from '../message-list/message-list.component';
import {Subscription} from "rxjs/Rx";

@Component({
  moduleId: module.id,
  selector: 'app-vote-detail',
  templateUrl: 'vote-detail.component.html',
  styleUrls: ['vote-detail.component.css'],
  directives: [IdeaListComponent, MessageListComponent, BubbleComponent]
})
export class VoteDetailComponent implements OnInit {
  vote: IVote;
  visible: boolean = false;
  paramSubscription: Subscription;
  iPhase: any = IPhase;

  constructor(private dataService: DataService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.paramSubscription = this.route.params.map(p => p['id']).combineLatest(this.dataService.votes)
      .subscribe(input => {
        let idFromUrl: string = input[0];
        let voteList: IVote[] = input[1];
        this.vote = voteList.find(v => v.id.toString() === idFromUrl);
        if (this.vote && this.vote.currentPhase === IPhase.PLANNED) {
          this.visible = true;
        }
      });
  }

  ngOnDestroy() {
    this.paramSubscription.unsubscribe();
  }

  navigateToEditVote() {
    this.router.navigate(['./vote', this.vote.id, 'edit']);
  }

  navigateToRemoveVote() {
    this.dataService.removeVote(this.vote);
    this.router.navigate(['./votes', {phase: IPhase.PLANNED}]);
  }
}
