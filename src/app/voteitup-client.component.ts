import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {DataService} from './shared/data.service';

@Component({
  moduleId: module.id,
  selector: 'voteitup-client-app',
  templateUrl: 'voteitup-client.component.html',
  styleUrls: ['voteitup-client.component.css'],
  encapsulation: ViewEncapsulation.None // Export this css to all components
})
export class VoteitupClientAppComponent implements OnInit {

  constructor(private dataService: DataService) {
  }

  ngOnInit(): any {
  }
}
