import {NgModule}       from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {VoteitupClientAppComponent}   from './voteitup-client.component';
import {routing, appRoutingProviders} from './voteitup-client.routing';
import {DataService} from './shared/data.service';
import {voteReducer} from './reducers/voteReducer';
import {messageReducer} from './reducers/messageReducer';
import {ideaReducer} from './reducers/ideaReducer';
import {provideStore} from '@ngrx/store';
import {VoteListComponent} from './feature/vote-list/vote-list.component';
import {VoteDetailComponent} from './feature/vote-detail/vote-detail.component';
import {VoteNewComponent} from './feature/vote-new/vote-new.component';
import {IdeaNewComponent} from './feature/idea-new/idea-new.component';
import {LoginComponent} from './feature/login/login.component';
import {AuthGuard} from './shared/auth-guard.service';

@NgModule({
  declarations: [VoteitupClientAppComponent, VoteListComponent,
    VoteDetailComponent, VoteNewComponent, IdeaNewComponent, LoginComponent],
  imports: [BrowserModule,
    FormsModule,
    routing],
  providers: [
    appRoutingProviders,
    AuthGuard,
    DataService,
    provideStore({votes: voteReducer, messages: messageReducer, ideas: ideaReducer})
  ],
  bootstrap: [VoteitupClientAppComponent],
})
export class VoteitupClientModule {
}
