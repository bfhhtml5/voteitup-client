import {IUser} from './IUser';
import {IUpvote} from './IUpvote';
export interface IIdea {
  id: number;
  voteId: number;
  name: string;
  createdBy: IUser;
  upVotes: IUpvote[];
}
