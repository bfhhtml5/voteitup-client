import {IVote} from './IVote';
import {IMessage} from './IMessage';
import {IIdea} from './IIdea';
export interface IStore {
  votes: IVote[];
  messages: IMessage[];
  ideas: IIdea[];
}
