import {IUser} from './IUser';
export interface IUpvote {
  createdBy: IUser;
  createdAt: Date;
}
