import {IUser} from './IUser';
export interface IMessage {
  id: number;
  voteId: number;
  createdBy: IUser;
  createdAt: Date;
  message: string;
  isSystemMessage: boolean;
}
