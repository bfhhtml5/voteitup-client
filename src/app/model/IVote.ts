import {IUser} from './IUser';
import {IPhase} from './IPhase';

export interface IVote {
  id: number;
  name: string;
  description: string;
  createdBy: IUser;
  ideasTo: Date;
  votesTo: Date;
  votesPerUser: number;
  imageUrl: string;
  currentPhase: IPhase;
}
