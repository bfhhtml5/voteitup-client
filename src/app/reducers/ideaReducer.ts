import {ActionReducer, Action} from '@ngrx/store';
import {IIdea} from '../model/IIdea';

export const ADD_IDEA = 'ADD_IDEA';
export const UPDATE_IDEA = 'UPDATE_IDEA';
export const INIT_IDEAS_FROM_SERVER = 'INIT_IDEAS_FROM_SERVER';

export const ideaReducer: ActionReducer<IIdea[]> =
  (state: IIdea[] = [], action: Action) => {

  switch (action.type) {
    case INIT_IDEAS_FROM_SERVER:
      return action.payload;

    case ADD_IDEA:
      return [...state, action.payload];

    case UPDATE_IDEA:
      return state.map(idea => {
        return idea.id === action.payload.id
          ? Object.assign({}, idea, action.payload)
          : idea;
      });

    default:
      return state;
  }
};
