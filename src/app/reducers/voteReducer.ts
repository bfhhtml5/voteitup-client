import { ActionReducer, Action } from '@ngrx/store';
import {IVote} from "../model/IVote";

export const ADD_VOTE = 'ADD_VOTE';
export const DELETE_VOTE = 'DELETE_VOTE';
export const UPDATE_VOTE = 'UPDATE_VOTE';
export const INIT_VOTES_FROM_SERVER = 'INIT_VOTES_FROM_SERVER';

export const voteReducer: ActionReducer<IVote[]> = (state: IVote[] = [], action: Action) => {
  switch (action.type) {
    case INIT_VOTES_FROM_SERVER:
      return action.payload;

    case ADD_VOTE:
      return [...state, action.payload];

    case DELETE_VOTE:
      return state.filter(v => v.id !== action.payload.id);

    case UPDATE_VOTE:
      return state.map(v => {
        return v.id === action.payload.id
          ? Object.assign({}, v, action.payload)
          : v;
      });

    default:
      return state;
  }
};

