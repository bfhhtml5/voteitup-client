import {ActionReducer, Action} from '@ngrx/store';
import {IMessage} from '../model/IMessage';

export const ADD_MESSAGE = 'ADD_VOTE';
export const INIT_MESSAGES_FROM_SERVER = 'INIT_MESSAGES_FROM_SERVER';

export const messageReducer: ActionReducer<IMessage[]> =
  (state: IMessage[] = [], action: Action) => {

  switch (action.type) {
    case INIT_MESSAGES_FROM_SERVER:
      action.payload.forEach(m => {
        m.createdAt = Date.parse(m.createdAt);
      });
      action.payload.reverse();
      return action.payload;

    case ADD_MESSAGE:
      action.payload.createdAt = Date.parse(action.payload.createdAt);
      return [action.payload, ...state];

    default:
      return state;
  }
};

