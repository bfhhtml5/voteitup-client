import {Injectable}     from '@angular/core';
import {CanActivate, Router}    from '@angular/router';
import {DataService} from './data.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private dataService: DataService, private router: Router) {
  }

  canActivate() {
    if (this.dataService.currentUser !== null) {
      return true;
    }

    // Navigate to the login page
    this.router.navigate(['/login']);
    return false;
  }
}
