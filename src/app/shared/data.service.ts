import {Injectable} from '@angular/core';
import * as io from 'socket.io-client';
import {Store} from '@ngrx/store';
import {ADD_VOTE, INIT_VOTES_FROM_SERVER, UPDATE_VOTE, DELETE_VOTE} from '../reducers/voteReducer';
import {Observable} from 'rxjs/Observable';
import {IStore} from '../model/IStore';
import {IVote} from '../model/IVote';
import {IIdea} from '../model/IIdea';
import {INIT_MESSAGES_FROM_SERVER, ADD_MESSAGE} from '../reducers/messageReducer';
import {INIT_IDEAS_FROM_SERVER, ADD_IDEA, UPDATE_IDEA} from '../reducers/ideaReducer';
import {IMessage} from '../model/IMessage';
import {IUser} from '../model/IUser';

@Injectable()
export class DataService {
  socket: any;
  votes: Observable<IVote[]>;
  messages: Observable<IMessage[]>;
  ideas: Observable<IIdea[]>;
  private currUser: IUser;

  constructor(private store: Store<IStore>) {
    let service = this;
    service.votes = store.select<IVote[]>('votes');
    service.messages = store.select<IMessage[]>('messages');
    service.ideas = store.select<IIdea[]>('ideas');

    service.attachToSocketEvents(service);
  }

  get currentUser() {
    if (!this.currUser) {
      // trying to get user from localStorage
      let userInStorage = window.localStorage.getItem('username');

      if (userInStorage) {
        this.currUser = { name: userInStorage };
        return this.currUser;
      } else {
        return null;
      }
    } else {
      return this.currUser;
    }
  }

  setCurrentUser(user: IUser) {
    this.currUser = user;
    window.localStorage.setItem('username', user.name);
  }

  attachToSocketEvents(service) {
    service.socket = io('https://voteitup-server.herokuapp.com', {});

    // Bind to events from server
    service.socket.on('init_votes', function (data) {
      service.store.dispatch({type: INIT_VOTES_FROM_SERVER, payload: data});
    });
    service.socket.on('init_messages', function (data) {
      service.store.dispatch({type: INIT_MESSAGES_FROM_SERVER, payload: data});
    });
    service.socket.on('init_ideas', function (data) {
      service.store.dispatch({type: INIT_IDEAS_FROM_SERVER, payload: data});
    });
    service.socket.on('new_vote', function (data) {
      service.store.dispatch({type: ADD_VOTE, payload: data});
    });
    service.socket.on('update_vote', function (data) {
      service.store.dispatch({type: UPDATE_VOTE, payload: data});
    });
    service.socket.on('remove_vote', function (data) {
      service.store.dispatch({type: DELETE_VOTE, payload: data});
    });
    service.socket.on('new_message', function (data) {
      service.store.dispatch({type: ADD_MESSAGE, payload: data});
    });
    service.socket.on('new_idea', function (data) {
      service.store.dispatch({type: ADD_IDEA, payload: data});
    });
    service.socket.on('update_idea', function (data) {
      service.store.dispatch({type: UPDATE_IDEA, payload: data});
    });
  }

  newVote(newVote: IVote) {
    this.socket.emit('client_new_vote', newVote);
  }

  removeVote(vote: IVote) {
    this.socket.emit('client_remove_vote', vote);
  }

  updateVote(vote: IVote) {
    this.socket.emit('client_update_vote', vote);
  }

  voteUp(voteId: number, ideaId: number) {
    this.socket.emit('client_vote_up', {
      voteId, ideaId, voter: this.currentUser
    });
  }

  voteDown(voteId: number, ideaId: number) {
    this.socket.emit('client_vote_down', {
      voteId, ideaId, voter: this.currentUser
    });
  }

  newMessage(message, voteId) {
    this.socket.emit('client_new_message', {
      message, voteId, createdBy: this.currentUser
    });
  }

  newIdea(newIdea: IIdea, voteId) {
    this.socket.emit('client_new_idea', {
      voteId: voteId,
      newIdea: newIdea,
    });
  }

  removeIdea(idea: IIdea) {
    this.socket.emit('client_remove_idea', idea);
  }

  updateIdea(idea: IIdea) {
    this.socket.emit('client_update_idea', idea);
  }
}
