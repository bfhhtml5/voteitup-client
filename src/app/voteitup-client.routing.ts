import { Routes, RouterModule } from '@angular/router';
import {VoteListComponent} from './feature/vote-list/vote-list.component';
import {VoteNewComponent} from './feature/vote-new/vote-new.component';
import {VoteDetailComponent} from './feature/vote-detail/vote-detail.component';
import {IdeaNewComponent} from './feature/idea-new/idea-new.component';
import {LoginComponent} from './feature/login/login.component';
import {AuthGuard} from './shared/auth-guard.service';

const appRoutes: Routes = [
  {path: 'votes', component: VoteListComponent, canActivate: [AuthGuard]},
  {path: 'vote/new', component: VoteNewComponent, canActivate: [AuthGuard]},
  {path: 'vote/:id', component: VoteDetailComponent, canActivate: [AuthGuard]},
  {path: 'vote/:id/edit', component: VoteNewComponent, canActivate: [AuthGuard]},
  {path: 'vote/:id/idea/new', component: IdeaNewComponent, canActivate: [AuthGuard]},
  {path: 'vote/:id/idea/:ideaId/edit', component: IdeaNewComponent, canActivate: [AuthGuard]},
  {path: 'vote/:id/new-idea', component: IdeaNewComponent, canActivate: [AuthGuard]},
  {path: 'login', component: LoginComponent},
  {path: '**', component: VoteListComponent, canActivate: [AuthGuard]}
];

export const appRoutingProviders: any[] = [
];

export const routing = RouterModule.forRoot(appRoutes, { useHash: true });
