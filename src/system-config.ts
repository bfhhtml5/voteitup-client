/***********************************************************************************************
 * User Configuration.
 **********************************************************************************************/
/** Map relative paths to URLs. */
const map: any = {};

/** User packages configuration. */
const packages: any = {
  'ng2-charts': {main: 'ng2-charts.js', defaultExtension: 'js'},
  'chartjs': { defaultExtension: 'js', format: 'cjs' }
};

////////////////////////////////////////////////////////////////////////////////////////////////
/***********************************************************************************************
 * Everything underneath this line is managed by the CLI.
 **********************************************************************************************/
const barrels: string[] = [
  // Angular specific barrels.
  '@angular/core',
  '@angular/common',
  '@angular/compiler',
  '@angular/forms',
  '@angular/http',
  '@angular/router',
  '@angular/platform-browser',
  '@angular/platform-browser-dynamic',

  // Thirdparty barrels.
  'rxjs',
  '@ngrx/core',
  '@ngrx/store',

  // App specific barrels.
  'app',
  'app/shared',
  'app/feature/vote-list',
  'app/feature/vote-detail',
  'app/feature/vote-new',
  'app/feature/message-new',
  'app/feature/message-list',
  'app/feature/idea-new',
  'app/feature/bubble',
  'app/feature/idea-list',
  'app/feature/login'
  /** @cli-barrel */
];

const cliSystemConfigPackages: any = {};
barrels.forEach((barrelName: string) => {
  cliSystemConfigPackages[barrelName] = {main: 'index'};
});

/** Type declaration for ambient System. */
declare var System: any;

// Apply the CLI SystemJS configuration.
System.config({
  map: {
    '@angular': 'vendor/@angular',
    'rxjs': 'vendor/rxjs',
    'main': 'main.js',
    'socket.io-client': 'vendor/socket.io-client/socket.io.js',
    '@ngrx': 'vendor/@ngrx',
    'ng2-charts': 'vendor/ng2-charts',
    'chartjs': 'vendor/chart.js/dist/Chart.bundle.min.js',
    'color-name': 'vendor/color-name/index.js',
    'color-convert': 'vendor/color-convert/index.js',
    'chartjs-color': 'vendor/chartjs-color/dist/color.js',
    'chartjs-color-string': 'vendor/chartjs-color-string/color-string.js',
    'moment': 'vendor/moment/moment.js'
  },
  packages: cliSystemConfigPackages,
});

// Apply the user's configuration.
System.config({map, packages});
